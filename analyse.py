import json
import spacy
from collections import Counter
import nltk
from nltk.corpus import wordnet
nltk.download('wordnet')

offensive_words = ['hate', 'fck','fucking','nude', 'rub','naked', 'hate', 'fuck', 'slut', 'whore', 'porn','ugly', 'bigboob', 'rapist', 'slutbag','naked', 'pig', 'pervert','butt', 'pussy','penis','fuckin','pantie', 'insult','gross','busty','rubbish','cum','cumdump', 'dumb','ass', 'dumbass','kink','humiliate','dirty','incest','impregnate','moron', 'violate','jerk','rape','terrorist','stupidity','pervert','gtfo','genitalia','gangbang','chastity','sexualize','femdom', 'feminize','bareback','sissyslut','sissy','screw','shitty','cis','degrade', 'homeerotic','erotic','suicide', 'swallow', 'dick','misogyny', 'suffer','race','nipple','slave','gross','breed','loser','sissification','fat','sexual', 'harrasement','bitch','strip','nigga','TW','insult','boob','submissive','mental', 'cunt', 'hoe','racist', 'sexist', 'homophobic', 'ableist', 'islamophobic', 'xenophobic']

def get_prefixes(word):
    return [word[:i] for i in range(1, len(word)+1)]

def check_offensive_word(tweet):
    words = nltk.word_tokenize(tweet)
    for word in words:
        if word.lower() in offensive_words:
            return True
        prefixes = get_prefixes(word.lower())
        for prefix in prefixes:
            if prefix in offensive_words:
                return True
    return False

while True:
    tweet = input("Enter a tweet (or 'quitter' to quit): ")
    if tweet.lower() == 'quitter':
        break
    offensive_word = check_offensive_word(tweet)
    if offensive_word:

        print(f'tweet :{tweet} \n hard label : YES')
    else:
        print(f'tweet :{tweet} \n hard label : NO')


