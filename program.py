import json
import spacy
from collections import Counter

with open('training_task1_gold_hard.json', 'r') as hardYes:
    data = json.load(hardYes)

result_ids = []
for id, info in data.items():
    if info.get('hard_label') == 'YES':
        result_ids.append(id)

with open('tweets.json', encoding='utf-8') as tweet:
    data = json.load(tweet)

nlp = spacy.load('en_core_web_sm')

lemmas = []
for key in data:
    id = data[key]["id_EXIST"]
    lang = data[key]["lang"]
    tweet = data[key]["tweet"]
    if lang == "en" and id in result_ids:
        doc = nlp(tweet)
        lemmas += [token.lemma_ for token in doc if not token.is_stop and token.is_alpha]

root_counts = Counter()
for lemma in lemmas:
    root = nlp(lemma)[0].lemma_
    root_counts[root] += 1

repeated_roots = {root: count for root, count in root_counts.items() if count > 1}
sorted_roots = sorted(repeated_roots.items(), key=lambda x: x[1], reverse=True)

for root, count in sorted_roots:
    print(f"{root}: {count}")
    #pass




"""def check_offensive(tweet, repeated_roots):
    tweet_words = tweet.split()
    for word in tweet_words:
        if word.lower() in repeated_roots:
            return word.lower()
    return None

while True:
    tweet = input("Enter a tweet (or 'quitter' to quit): ")
    if tweet.lower() == 'quitter':
        break
    offensive_word = check_offensive(tweet, repeated_roots)
    if offensive_word:
        print(f"YES, {offensive_word} is offensive.")
    else:
        print("NO")
"""

